<?php include('header.php'); ?>
<body>
	<div class="landing-slides-wrapper">

			<div class="video-wrapper">
				<div id="skip-video">
					<div class="skip-video">skip video<span class="btn-skip"></span></div>
					<video id="landing-video" autoplay>
						<source src="assets/images/video-trim-nosound_2.mov" type="video/mp4" />	
					</video>
				</div>
			</div>

		
		<a href="page-couture-index.php">
			<div class="landing-image landing-couture">
				<div class="shade"></div>
				<div class="overall-entrance-wrapper">
					<div class="landing-logo-couture-w landing-logo"></div>
					<hr>
					<div class="landing-entrance-wrapper">
						<img src="assets/images/btn-left-entrance.svg" style="left: 15px;">
						<div class="lash-couture-lash">SUAVIS COUTURE</div>
					</div>
				</div>
			</div>
		</a>
		<a href="page-lashbar-index.php">
			<div class="landing-image landing-lashbar">
				<div class="shade"></div>
				<div class="overall-entrance-wrapper">
					<div class="landing-logo-lashbar-w landing-logo"></div>
					<hr>		
					<div class="landing-entrance-wrapper">
						<img src="assets/images/btn-right-entrance.svg" style="right: 15px;">
						<div class="lash-couture-lash">SUAVIS LASH BAR</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</body>
</html>