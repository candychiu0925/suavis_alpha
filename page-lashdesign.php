<?php include('header.php'); ?>
<body data-spy="scroll" data-target="#navbar-example">
	<div class="single-wrapper single-couture-wrapper">
		<div class="container-fluid">
			<?php include('navigation.php'); ?>
			<?php include('mobile-nav.php'); ?>
			<div class="clearfix"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-2 hidden-xs hidden-sm">
						<div class="menu-anchor" id="scrollspy">
							<ul class="nav">
								<li><a href="#lashdesign">LASH DESIGN</a></li>
							</ul>
						
							<ul class="nav">
								<li><a href="#lashtype">LASH TYPE</a></li>
							</ul>
							
							<ul class="nav">
								<li><a href="#volume">LASH VOLUME</a></li>
							</ul>
							
							<ul class="nav">
								<li><a href="#curvature">LASH DETAILS</a></li>
								<li><a href="#curvature">CURVATURE</a></li>
								<li><a href="#thickness">THICKNESS</a></li>
								<li><a href="#length">LENGTH</a></li>
							</ul>
						</div>
					</div>
				
					<div class="col-xs-12 col-md-10">
					
					<div class="row">
						<div id="lashdesign">
							<div class="col-xs-12">
								<h2>LASH DESIGN</h2>
							</div>
							<div class="clearfix"></div>
							<div class="valign-wrapper">
								<div class="col-xs-12 col-sm-6">
									<div id="img-slider">
										<img src="assets/images/lashbefore.jpg">
										<img src="assets/images/lashafter.jpg">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 valign">
									<p>Regardless you have Hooded Eyes, Monolids, Almond Eyes or Up/Down turned eyes. Different Lashes design maximize (bring out) the best look and hide the weakness..</p>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-xs-12 valign-wrapper lashtype-volume">
							<div class="col-xs-12 col-md-5 lashdesign-spacing valign">
								<h3>LASH TYPE</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.</p>
								<a href="#lashtype">
									<div class="btn-lashtype">LEARN MORE</div>
								</a>
							</div>
							<div class="col-xs-12 col-md-2">
								<div class="lashdesign-cross valign"></div>
							</div>
							<div class="col-xs-12 col-md-5 lashdesign-spacing valign">
								<h3>VOLUME</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.</p>
								<a href="#volume">
									<div class="btn-lashtype">LEARN MORE</div>
								</a>
							</div>
						</div>
						
						<div class="lashdesign-spacing col-xs-12" id="lashtype">
							<div class="col-xs-12">
								<hr>
								<h3>LASH TYPE</h3>
							</div>
							<div class="clearfix"></div>
							<div class="lashtype-spacing valign-wrapper" id="tulle">
								<div class="col-xs-12 col-sm-6">
									<div class="lashtype-eye-image lashtype-eye-image1"></div>
									<h4>TULLE</h4>
									<p>A subtle and natural design to boost your lashes noticeable without looking artificial. A straighter curve and shorter length of lashes are applied around the upper eye. </p>
								</div>
								
								<div class="col-xs-12 col-sm-6">
									<div class="lashtype-eye-image lashtype-eye-image1"></div>
									<h4>MERMAID</h4>
									<p>A combination of the the sexy and elegant look.The longest lashes are placed at the outer corner of the eye. The cat eye effect enables the eyes look longer and wider. </p>
								</div>
								
							</div>
							
							
							<div class="lashtype-spacing valign-wrapper" id="peplum">
								<div class="col-xs-12 col-sm-6">
									<div class="lashtype-eye-image lashtype-eye-image1"></div>
									<h4>PEPLUM</h4>
									<p>Highlighting the center of the eye and make the pupil stand out.  Longer lashes are on the center of the upper eyelid and it makes the eye look rounder and bigger like a doll.</p>
								</div>

								<div class="col-xs-12 col-sm-6">
									<div class="lashtype-eye-image lashtype-eye-image1"></div>
									<h4>PRINCESS</h4>
									<p>A mixture of innocent sweet and sexy style. The longest lashes are adhere on the center of the eyelids and the mixture of the long to short length is gradually adhere towards to the end of the eye</p>
								</div>
							</div>
							

							<div class="lashtype-spacing"  id="empire">
								<div class="col-xs-12 col-sm-6">
									<div class="lashtype-eye-image lashtype-eye-image1"></div>
									<h4>EMPIRE</h4>
									<p>A red carpet glamorous and luscious style. Emphasise in thicker curve and fuller volume. Longer length of lashes are applied all around the eye. </p>
								</div>
								
								<div class="col-xs-12 col-sm-6">
									<div class="lashtype-eye-image lashtype-eye-image1"></div>
									<h4>GODET</h4>
									<p>Twinkle twinkle like a star! Multi lashes length of long and short extensions are placed unevenly across the eye to create a stunning look.</p>
								</div>
							</div>
							
							
						</div>
						<div class="clearfix"></div>
						
						<div class="lashdesign-spacing lashdesign-section" id="volume">
							<div class="col-xs-12">
								<h3>VOLUME</h3>
								<div class="lashtype-spacing">
									<div class="lashdetails lashdetails-volume hidden-xs hidden-sm"></div>
									<img src="assets/images/volume-mobile.svg" alt="" / class="hidden-md hidden-lg">
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						
						<div class="lashdesign-spacing lashdesign-section">
							<div class="col-xs-12">
								<hr>
								<h3>LASH DETAILS</h3>
								<div class="lashtype-spacing" id="curvature">
									<h5>CURVATURE</h5>
									<div class="middle-section-decor"></div>
									<div class="lashtype-spacing">
										<div class="lashdetails lashdetails-curvature hidden-xs hidden-sm"></div>
										<img src="assets/images/curvature-mobile.svg" alt="" / class="hidden-md hidden-lg">
									</div>
								</div>
								<div class="clearfix"></div>
								
								<div class="lashtype-spacing" id="thickness">
									<h5>THICKNESS</h5>
									<div class="middle-section-decor"></div>
									<div class="lashdetails lashdetails-thickness hidden-xs hidden-sm"></div>
									<img src="assets/images/thickness-mobile2.svg" alt="" / class="hidden-md hidden-lg">
								</div>
								
								<div class="lashtype-spacing" id="length">
									<h5>LENGTH</h5>
									<div class="middle-section-decor"></div>
									<div class="lash-spacing">
										<div class="lashdetails lashdetails-length hidden-xs hidden-sm"></div>
									
										<img src="assets/images/length-mobile.svg" alt="" /class="hidden-md hidden-lg">
									</div>
								</div>
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<?php include('footer.php'); ?>
	
	
</body>
</html>