<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<title>SUAVIS</title>
	
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	
	
	<link rel="stylesheet" href="assets/css/twentytwenty.css">
	
	
	
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="assets/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/css/jquery-ui.theme.min.css">
	<link rel="stylesheet" href="assets/css/jquery-ui.structure.min.css">
	<link rel="stylesheet" href="assets/css/foundation.css">
	<link rel="stylesheet" href="assets/css/styles.css">
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHacpwy62KP0_hgJAa1CtOaPMyBpUdh1g"></script>
	<script src="assets/js/jquery-1.11.3.min.js"></script>
	<script src="assets/js/jquery.event.move.js"></script>
	<script src="assets/js/jquery.twentytwenty.js"></script>
	<script src="assets/js/imagesloaded.pkgd.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	
	<script src="assets/js/main.js"></script>
	<script src="assets/js/g-map.js"></script>
	<script src="https://use.typekit.net/bij0oal.js"></script>
	<script src="assets/js/jquery.focuspoint.min.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
