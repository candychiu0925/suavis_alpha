<?php include('header.php'); ?>
<body>
	<div class="single-wrapper">
		<?php include('navigation-lashbar.php'); ?>
		<?php include('mobile-nav-lashbar.php'); ?>
		<div class="container contactus-wrapper">
			<div class="row">
				<div class="col-xs-12">
					<h2>CONTACT US</h2>
					<div class="middle-section-decor"></div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12 col-md-6">
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<h5>Address</h5>
						</div>
						<div class="col-md-6 col-xs-12">
							<p>8/F, 1 Lan Kwai Fong,
							Central, Hong Kong</p>
						</div>
						
						<div class="col-md-6 col-xs-12">
							<h5>Telephone</h5>
						</div>
						<div class="col-md-6 col-xs-12">
							<p>+852 2336 6328</p>
						</div>
						
						<div class="col-md-6 col-xs-12">
							<h5>Email Address</h5>
						</div>
						<div class="col-md-6 col-xs-12">
							<p>info@suavislash.com</p>
						</div>
						
						<div class="col-md-6 col-xs-12">
							<h5>Opening Hours</h5>
						</div>
						<div class="col-md-6 col-xs-12">
							<p>Monday to Friday<br>
							11 am to 8 pm <br>
							Last Appointment at 7 pm</p>
							<p>Saturday<br>
							10 am to 6 pm <br>
							Last Appointment at 5 pm</p>
							<p>Sunday Closed</p>
						</div>
						
						
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div id="map" class="map"></div>
				</div>
			</div>
		</div>
		<div class="push"></div>
	</div>
	<?php include('footer-lash.php'); ?>
	
	
</body>
</html>