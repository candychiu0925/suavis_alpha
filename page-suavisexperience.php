<?php include('header.php'); ?>
<body>
	<div class="single-wrapper single-couture-wrapper suavisexp-wrapper">
		
		<?php include('navigation.php'); ?>
		
		<?php include('mobile-nav.php'); ?>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2>THE SUAVIS EXPERIENCE</h2>
				</div>
				<div class="clearfix"></div>
				<div class="suavisexp-spacing valign-wrapper">
					<div class="col-xs-12 col-sm-6">
						<div class="suavisexp-step1-image suavisexp-step1-image1"></div>
	
					</div>
				
					<div class="col-xs-12 col-sm-offset-1 col-sm-5 valign">
						<h4>STEP 1.</h4>
						<p>Tell us about your eyelash extension experience, health condition and your concern by filling out customer questionnaire. Our stylist will conduct a consultation and discuss the style, thickness, curvature, qualities with you</p>
					</div>
					
				</div>
				<div class="clearfix"></div>
				
				<div class="suavisexp-spacing valign-wrapper">
				
					<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
						<div class="suavisexp-step1-image suavisexp-step1-image2"></div>
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5 valign">
						<h4>STEP 2.</h4>
						<p>Stylist will start by separating your upper and lower lashes and placing an eye mask on your under eye area to avoid them sticking together. Cleansing procedure will be started.</p>
					</div>
					<div class="col-sm-offset-1 col-sm-6 hidden-xs">
						<div class="suavisexp-step1-image suavisexp-step1-image2"></div>
					</div>
				</div>
				<div class="clearfix"></div>
				
				
				<div class="suavisexp-spacing valign-wrapper">
					<div class="col-xs-12 col-sm-6">
						<div class="suavisexp-step1-image suavisexp-step1-image3"></div>
	
					</div>
				
					<div class="col-xs-12 col-sm-offset-1 col-sm-5 valign">
						<h4>STEP 3.</h4>
						<p>Each synthetic lash is meticulously grafted to 1 - 2 mm apart from the root of each individual natural lash. The process will take 60 to 120 minutes.</p>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="suavisexp-spacing valign-wrapper">
					<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
						<div class="suavisexp-step1-image suavisexp-step1-image4"></div>
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5 valign">
						<h4>STEP 4.</h4>
						<p>After application is completed. Air compressor with cool air will be used to dry the adhesive. Home care kit and after care leaflet will be provided when you check out.</p>
					</div>
					<div class="col-sm-offset-1 col-sm-6 hidden-xs">
						<div class="suavisexp-step1-image suavisexp-step1-image4"></div>
					</div>
				</div>
				<div class="clearfix"></div>
				
			</div>
		</div>	
	</div>
	<?php include('footer.php'); ?>
	
	
</body>
</html>