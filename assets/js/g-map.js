var mapStyles = [{"featureType":"all","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-100},{"lightness":0}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]}]

var suavisLatLng = { lat: 22.280963, lng: 114.155667};

function initMap() {
    var suavisLatLng = {lat: 22.280963, lng: 114.155667};
    var suavismap = new google.maps.Map(document.getElementById('map'), {
      zoom: 18,
      center: suavisLatLng,
      mapTypeControl: false,
      streetViewControl: false,
      scrollwheel: false,
      styles: mapStyles
    });
    
    
    var marker = new google.maps.Marker({
      position: suavisLatLng,
      map: map
    });
    
    var image = {url:"assets/images/map-icon-couture@2x.png",
    scaledSize: new google.maps.Size(81.59, 92.72)};
    var marker = new google.maps.Marker({
    	position: suavisLatLng,
    	map: suavismap,
    	icon: image
    });
    
  }
 
 
 
// check map start
 google.maps.event.addDomListener(window, 'load', function(){
 	if($('#map').length > 0){
 		initMap();
 	}
 });