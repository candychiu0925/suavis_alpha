var ww,wh;
$(function(){
	checkResize();
	$(window).resize(checkResize);
	
	$('body').scrollspy({
		target: '#scrollspy',
		offset: $('#nav-default').outerHeight(true)
	});
	
	$('#scrollspy .nav a').click(function(e){
		e.preventDefault();
		var id = $(this).attr('href');
		$('body,html').animate({
			scrollTop: $(id).offset().top - $('#nav-default').outerHeight(true)
		});
	});
//	$(window).scroll(function(){
//		us();
//		console.log(ws);
//	});
	
	$('#datepicker').datepicker({
	 	 dayNamesMin: [ "S", "M", "T", "W", "T", "F", "S" ]
	});
	
	if($('#landing-video').length > 0){
	
		$('#skip-video').click(function(){
			$('#skip-video').hide();
			$('.landing-image').addClass('showup');
		});
	
		var video = document.getElementById('landing-video');
		video.onended = function(){
			$('#skip-video').hide();
			$('.landing-image').addClass('showup');
		};
	}
	
	
	if($('#img-slider').length > 0){
		$('#img-slider').twentytwenty();
	}
	
	$('.btn-mobile-nav').click(function(){
		$('.mobile-nav-menu').addClass('on');
	})
	
	$('.btn-mobile-close').click(function(){
		$('.mobile-nav-menu').removeClass('on');
	})
	
	
	
	
	$(window).scroll(function(){
		us();
		
//		if(ws > $('#suavis-top').outerHeight()){
//			if(!$('#nav-default').hasClass('hold')){
//				$('#nav-default').addClass('hold');
//			}
//		}else{
//			if($('#nav-default').hasClass('hold')){
//				$('#nav-default').removeClass('hold');
//			}
//		}
		
		if(ws > $('#suavis-top').outerHeight() + $('#nav-default').outerHeight()){
			($('#nav-default1').addClass('hold'))
		}else{
				$('#nav-default1').removeClass('hold');
			}
	});
	
});

function checkResize(){
	us();
	
	
	
	
	if(ww >= 768){
				
		$('.valign').each(function(){
			var $text = $(this);
			var $parent = $text.closest('.valign-wrapper');
			
			$parent.imagesLoaded(function(){
				$text.css({
					position: 'relative',
					top: $parent.height()/2 - $text.height()/2
				});
			});
			
		});
	}else {
		$('.valign').attr('style','');
	}
	
	resizeVideo();
}

function resizeVideo(){
	var videoRatio = 1080/1920;
	var screenRatio = wh/ww;
	var newH = 0;
	var newW = 0;
	
	if(screenRatio > videoRatio && ww > 768){
		newH = wh;
		newW = wh / videoRatio;
	}else {
		newW = ww;
		newH = ww * videoRatio;
	}
	
	$('#landing-video').css({
		width: newW,
		height: newH,
		left: ww/2 - newW/2,
		top: wh/2 - newH/2
	});
}

function us() {
	ww = window.innerWidth;
	wh = window.innerHeight;
	ws = $(window).scrollTop();
}