var canvas;
var ctx;

window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	function( callback ){
		window.setTimeout(callback, 1000 / 60);
	};
})();

$(function(){

	canvas = document.getElementById('canvas');
	if(canvas){
		ctx = canvas.getContext('2d');			
		animate();	
		$(window).resize(function(){
			checkResize();
		});
	}
	
});

function animate(){
	draw();
	requestAnimFrame(function() {
		animate();
	});
}

function draw(){

}

function checkResize(){

}

function degToRad(deg){
	return deg*(Math.PI/180);
}
function radToDeg(rad){
	return rad*(180/Math.PI);
}