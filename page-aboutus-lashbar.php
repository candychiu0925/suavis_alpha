<?php include('header.php'); ?>
<body>
	<div class="single-wrapper aboutus-wrapper">
		<?php include('navigation-lashbar.php'); ?>
		<?php include('mobile-nav-lashbar.php'); ?>
		<div class="col-xs-12 aboutus-top aboutus-top1"></div>
		<div class="container-fluid">
			<div class="clearfix"></div>
			<div class="container">
				<div class="row">
					<div class="aboutus-spacing valign-wrapper">
						<div class="col-xs-12 col-sm-6 valign">
							<h2>ABOUT SUAVIS</h2>
							<p>We, as eyelash lovers, are always eager to find ways to accentuate our eyes. At Suavis, we employ the trusted Japanese technique in eyelash extension that is harmless and natural, even without makeup. We aim to provide only the best professional eyelash extension services to Cater for your sensitive eyes.</p>
						</div>
						<div class="col-xs-12 col-sm-6">
							<img src="assets/images/aboutus-lashbar2@2x.png">
						</div>
					</div>
					
					<div class="aboutus-spacing valign-wrapper">
						<div class="col-xs-12 col-sm-6 hidden-xs hidden-sm">
							<img src="assets/images/aboutus-lashbar3@2x.png">
						</div>
						
						<div class="col-xs-12 col-sm-6 valign">
							<h2>WHAT DOES SUAVIS DO?</h2>
							<p>Here at Suavis Lash, we specialise in enhancing your natural beauty by customizing your eyelash style to your specific requirements. With our specially trained stylists, you can be assumed that your eyes and lashes are in good hands.</p>
							
							<p>We have a deep passion for beauty and art, and we consider eyelash extension as not just a technique, but an artistry.</p>
							
							<p>With proper maintenance, we guarantee that our harmless procedure will keep your natural lashes full and healthy.</p>
						</div>
						
						<div class="col-xs-12 col-sm-6 hidden-md hidden-lg">
							<img src="assets/images/aboutus-lashbar3@2x.png">
						</div>
					</div>
					
					
					<div class="aboutus-spacing valign-wrapper">
						<div class="col-xs-12 col-sm-6 valign">
							<h2>WHY CHOOSE SUAVIS?</h2>
							<p>Our motto is simple - “Safety is our first priority”. The materials used at Suavis Lash have all been carefully selected, ensuring that only the safest and best quality products are applied on our customers, With the Japanese eyelash extension technique, each lash is meticulously grafted to each individual natural lash, leaving a 1mm to 2mm distance away from the root of the natural lashes.</p>
							
							<p>Which ever aura you prefer, whether it be Natural, Cute, Sexy or Glamorous, we here at Suavis can be your professional stylist and make your beauty dreams come true.</p>
						</div>
						<div class="col-xs-12 col-sm-6">
							<img src="assets/images/aboutus-lashbar14@2x.png">
						</div>
					</div>
					
					
					<div class="aboutus-spacing valign-wrapper">
						<div class="col-xs-12 col-sm-6 hidden-xs hidden-sm">
							<img src="assets/images/aboutus-lashbar5@2x.png">
						</div>
						
						<div class="col-xs-12 col-sm-6 valign">
							<h2>WHERE ARE WE?</h2>
							<p>Suavis Lash Bar is located in the heart and soul of Lan Kwai Fong, Central. Visit our cozy salon where we offer a tranquil hideaway in the hustle and bustle of the Central Area.</p>
						</div>
						
						<div class="col-xs-12 col-sm-6 hidden-md hidden-lg">
							<img src="assets/images/aboutus-lashbar5@2x.png">
						</div>
					</div>
					
				</div>
			</div>
		</div>
	
		
	
	</div>
	<?php include('footer-lash.php'); ?>
</body>
</html>