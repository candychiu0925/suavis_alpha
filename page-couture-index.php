<?php include('header.php'); ?>

<body>
	<div class="overall-wrapper landing-wrapper landing-couture-wrapper">
		
		<!--carousel start-->
		<div id="suavis-top">
			<div id="suavis-home-carousel" class="suavis-home-carousel carousel slide" data-ride="carousel">
				<a href="index.php">
					<div class="landing-logo landing-logo-couture-w hidden-sm hidden-xs"></div>
				</a>
				
			<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#suavis-home-carousel" data-slide-to="0" class="active"></li>
					<li data-target="#suavis-home-carousel" data-slide-to="1"></li>
					<li data-target="#suavis-home-carousel" data-slide-to="2"></li>
				</ol>
				
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<div class="home-top-section home-top-section-1"></div>
					</div>
					<div class="item">
						<div class="home-top-section home-top-section-2"></div>		
					</div>
					<div class="item">
						<div class="home-top-section home-top-section-3"></div>		
					</div>
					
				</div>
				
				<div class="carousel-title">
					<h1>SUAVIS COUTURE</h1>
					<p>The Best Eyelash Extension in Town</p>
				</div>
			</div>
		</div>
		<!--carousel start end-->
		
		<div class="mobile-nav hidden-md hidden-lg">
			<a href="page-couture-index.php">
				<div class="mobilenav-logo mobilenav-logo1"></div>
			</a>
			<div class="btn-mobile-nav"></div>
		</div>
		<div class="mobile-nav-menu hidden-md hidden-lg">
			<div class="mobilenav-logo mobilenav-logo1"></div>
			<ul>
				<a href="page-aboutus.php">
					<li>ABOUT US</li>
				</a>
				<a href="page-lashdesign.php">
					<li>LASH DESIGN</li>
				</a>
		
				<a href="page-suavisexperience.php">
					<li>THE SUAVIS EXPERIENCE</li>
				</a>
				<a href="page-contactus.php">
					<li>CONTACT US</li>
				</a>
				
			</ul>
			<div class="btn-mobile-close btn-mobile-close1"></div>
			<div class="sns-wrapper">
				<a href="#">
					<i class="fa fa-instagram lg" aria-hidden="true" style="font-size: 23px; line-height: 1;"></i>
				</a>
				<a href="#">
					<i class="fa fa-facebook lg" aria-hidden="true" style="font-size: 23px; line-height: 1;"></i	>
				</a>
			</div>
			
			<div class="mobile-reser-tc">
				<a href="#" type="button" data-toggle="modal" data-target="#myModal">
					<div class="mobile-reser">
						RESERVATION
					</div>
				</a>
						
				<div class="mobile-tc">
					繁
				</div>
			</div>
		
		</div>
		
		
		
		
		<?php include('navigation.php'); ?>
		
		<div id="nav-default1" class="navigation nav-couture hidden-sm hidden-xs">
			<div class="logo-navlink">
				<a href="page-couture-index.php">
					<div class="landing-logo landing-logo-couture-w"></div>
				</a>
				<ul>
					<a href="page-aboutus.php">
						<li>ABOUT US</li>
					</a>
					<a href="page-lashdesign.php">
						<li>LASH DESIGN</li>
					</a>
					
					<a href="page-suavisexperience.php">
						<li>THE SUAVIS EXPERIENCE</li>
					</a>
					<a href="page-contactus.php">
						<li style="margin-right: 0px;">CONTACT US</li>
					</a>
				</ul>
			</div>
			<div class="reservation-lang-wrapper">
				<a href="#" type="button" data-toggle="modal" data-target="#myModal">
					<div class="nav-reservation reservation-couture">RESERVATION</div>
				</a>
				<a href="#">
					<div class="nav-lang lang-couture">繁</div>
				</a>
			</div>
		</div>
		
		<div class="col-xs-12 middle-section">
			<div class="container">
				<div class="col-md-offset-1 col-md-5 row-desktop">
					<div class="middle-section-decor"></div>
					<h1>LASH DESIGN</h1>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy standard dummy standard dummy standard dummy standard dummy standard dummy standard dummy standard dummy standard dummy text ever since the 1500s...</p>
					<a href="page-lashdesign.php">
						<div class="btn-knowmore btn-knowmore1"></div>
					</a>
				</div>
				<div class="col-md-5 middle-section-image middle-section-image-1">
					<div class="shade"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12 middle-section2-image middle-section2-image-2">
			<div class="shade"></div>
			<div class="container">
				<div class="row">
					<div class="middle-section2-wrapper">	
						<div class="col-xs-12 col-md-6">
							<div class="row">
								<div class="middle-section-decor"></div>
								<h1>THE SUAVIS EXPERIENCE</h1>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since standard dummy text ever since standard dummy the 1500s</p>
								<a href="page-suavisexperience.php">
									<div class="btn-knowmore btn-knowmore1"></div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="landing-about-wrapper">
				<img src="assets/images/landing-about-us.png">
				<div class="landing-content-wrapper">
					<div class="container">
						<div class="col-md-offset-1 col-md-3 col-xs-12">
							<div class="landing-aboutlogo landing-about-lashlogo"></div>
						</div>
						<div class="col-md-7 col-xs-12">
							<div class="row">
								<p>Perfectionism is our philosophy! Our serenity and private ambiance give you a luxurious experience of the top lashes extension in town. Our stylists’ magician hands and tailored design treatment would turn your plain beauty into a Hollywood star glamour and turn people’s head when you walking down the street! </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php include('footer.php'); ?>
	</div>
</body>
</html>