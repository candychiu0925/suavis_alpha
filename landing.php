<?php include('header.php'); ?>
<body>
	
	<div class="col-md-6 landing-image landing-couture">
		<div class="shade"></div>
		<a href="page-couture-index.php">
			<div class="overall-entrance-wrapper">
				<div class="landing-logo-couture-w landing-logo"></div>
				<hr>
				<div class="landing-entrance-wrapper">
					<img src="assets/images/btn-left-entrance.svg" style="left: 15px;">
					<div class="lash-couture-lash">SUAVIS COUTURE</div>
				</div>
			</div>
		</a>
	</div>
	<div class="col-md-6 landing-image landing-lashbar">
		<div class="shade"></div>
		<a href="page-lashbar-index.php">
			<div class="overall-entrance-wrapper">
				<div class="landing-logo-lashbar-w landing-logo"></div>
				<hr>
				<div class="landing-entrance-wrapper">
					<img src="assets/images/btn-right-entrance.svg" style="right: 15px;">
					<div class="lash-couture-lash">SUAVIS LASH BAR</div>
				</div>
			</div>
		</a>
	</div>
	
</body>
</html>