<div class="clearfix"></div>
<div class="container-fluid" style="background-color: #fff;">
	<div class="footer">
		<ul>
			<li class="hidden-xs hidden-sm">Copyright © Suavis 2017. All rights reserved.</li>
			<a href="page-mediaexposure-lash.php">
				<li>Media Exposure</li>
			</a>
		</ul>
		<div class="sns-wrapper">
			<a href="https://www.instagram.com/suavislash/">
				<i class="fa fa-instagram lg" aria-hidden="true"></i>
			</a>
			<a href="https://www.facebook.com/Suavislash/">
				<i class="fa fa-facebook lg" aria-hidden="true"></i>
			</a>
		</div>
		
		<div class="hidden-md hidden-lg" style="font-size: 11px; line-height: 1; margin-top: 15px;">Copyright © Suavis 2017. All rights reserved.</div>
	</div>
</div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="btn-reser-close"  type="button" class="close" data-dismiss="modal" aria-label="Close"></div>
				<h4 class="modal-title col-xs-12" id="myModalLabel">RESERVATION</h4>
				<p class="col-md-7 col-xs-12">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s.</p>
			</div>
			<div class="clearfix"></div>
			<div class="modal-body">
				<div class="col-xs-12 col-md-6">
					<div id="datepicker"></div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="row">
						<div class="form">
							<div class="col-xs-12">
								<input type="text" id="text" placeholder="NAME" class="form-style" />
							</div>
							<div class="col-xs-12">
								<input type="email" id="email" placeholder="EMAIL" class="form-style" />
							</div>
							<div class="col-xs-12">
								<textarea type="text" id="comments" placeholder="SPECIAL REQUEST" class="form-style form-comments"></textarea>
							</div>
							<div class="col-xs-12">
								<a href="#">
									<div class="btn-submit">SUBMIT</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
