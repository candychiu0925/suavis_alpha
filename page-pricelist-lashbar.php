<?php include('header.php'); ?>
<body data-spy="scroll" data-target="#navbar-example">
	<div class="single-wrapper">
		<div class="container-fluid">
			<?php include('navigation-lashbar.php'); ?>
			<?php include('mobile-nav-lashbar.php'); ?>
			<div class="clearfix"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-2 hidden-xs hidden-sm">
						<div class="menu-anchor" id="scrollspy">
							<ul class="nav">	
								<li>PRICE LIST</li>
								<li><a href="#upper-eyelash">UPPER EYELASH</a></li>
								<li><a href="#lower-eyelash">LOWER EYELASH</a></li>
								<li><a href="#additional">ADDITIONAL OPTIONS</a></li>
							</ul>
						</div>
					</div>
					
					<div class="col-xs-12 col-md-10">
						<h2>PRICE LIST</h2>
						<div class="pricelist-spacing" id="upper-eyelash">
							<div class="middle-section-decor"></div>
							<h4>UPPER EYELASH</h4>
							<table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; margin: 0 auto;">	
								<thead>
									<th>QUANTITY</th>
									<th>JAPANESE TECHNIQUE</th>
									<th>3D Russian Volume (HKD)</th>
								</thead>
								<tbody>
									<tr>
										<td>80 (40+40)</td>
										<td>680</td>
										<td>1360</td>
									</tr>
									<tr>
										<td class="td-bgwhite">100 (50+50)</td>
										<td class="td-bgwhite">800</td>
										<td class="td-bgwhite">1360</td>
									</tr>
									<tr>
										<td>80 (40+40)</td>
										<td>680</td>
										<td>1360</td>
									</tr>
									<tr>
										<td class="td-bgwhite">100 (50+50)</td>
										<td class="td-bgwhite">800</td>
										<td class="td-bgwhite">1360</td>
									</tr>
									<tr>
										<td>80 (40+40)</td>
										<td>680</td>
										<td>1360</td>
									</tr>
									<tr>
										<td class="td-bgwhite">100 (50+50)</td>
										<td class="td-bgwhite">800</td>
										<td class="td-bgwhite">1360</td>
									</tr>
									<tr>
										<td>80 (40+40)</td>
										<td>680</td>
										<td>1360</td>
									</tr>
									
									
								</tbody>
							</table>
						</div>
						<div class="pricelist-spacing" id="lower-eyelash">
							<div class="middle-section-decor"></div>
							<h4>LOWER EYELASH</h4>
							<table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; margin: 0 auto;">	
								<thead>
									<th>QUANTITY</th>
									<th>JAPANESE TECHNIQUE</th>
									<th>3D Russian Volume (HKD)</th>
								</thead>
								<tbody>
									<tr>
										<td>80 (40+40)</td>
										<td>680</td>
										<td>1360</td>
									</tr>
									<tr>
										<td class="td-bgwhite">100 (50+50)</td>
										<td class="td-bgwhite">800</td>
										<td class="td-bgwhite">1360</td>
									</tr>
									<tr>
										<td>80 (40+40)</td>
										<td>680</td>
										<td>1360</td>
									</tr>
									<tr>
										<td class="td-bgwhite">100 (50+50)</td>
										<td class="td-bgwhite">800</td>
										<td class="td-bgwhite">1360</td>
									</tr>
									<tr>
										<td>80 (40+40)</td>
										<td>680</td>
										<td>1360</td>
									</tr>
									<tr>
										<td class="td-bgwhite">100 (50+50)</td>
										<td class="td-bgwhite">800</td>
										<td class="td-bgwhite">1360</td>
									</tr>
									<tr>
										<td>80 (40+40)</td>
										<td>680</td>
										<td>1360</td>
									</tr>
									
									
								</tbody>
							</table>
						</div>
						
						<div class="pricelist-spacing pricelist-additional" id="additional">
							<div class="middle-section-decor"></div>
							<h4>ADDITIONAL OPTIONS</h4>
							<table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; margin: 0 auto;">	
								<thead>
									<th>TYPES OF TREATMENTS</th>
									<th>TREATMENTS PRICE (HKD)</th>
								</thead>
								<tbody>
									<tr>
										<td>Eyelash Repair (per lash)</td>
										<td>680</td>
									</tr>
									<tr>
										<td class="td-bgwhite">3D Russian Volume (per lash)</td>
										<td class="td-bgwhite">800</td>
									</tr>
									<tr>
										<td>Colour Eyelash (per lash)</td>
										<td>680</td>
									</tr>
									<tr>
										<td class="td-bgwhite">Eyelash Removal</td>
										<td class="td-bgwhite">800</td>
									</tr>
									<tr>
										<td>Eyelash Regrowth Treatment</td>
										<td>680</td>
									</tr>
									<tr>
										<td class="td-bgwhite">RF Eye Lifting Treatment</td>
										<td class="td-bgwhite">800</td>
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			
		
		</div>
		
	
	</div>
	<?php include('footer-lash.php'); ?>
</body>
</html>